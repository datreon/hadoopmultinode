//Setting static ip
  sudo vi /etc/network/interfaces

  edit /etc/resolv.conf
$  echo " nameserver 8.8.8.8 " | sudo  tee /etc/resolv.conf
  sudo /etc/init.d/networking restart

//Disable IPV6
  sudo vi /etc/sysctl.conf
//Add following lines athe end
  net.ipv6.conf.all.disable_ipv6 = 1
  net.ipv6.conf.default.disable_ipv6 = 1
  net.ipv6.conf.lo.disable_ipv6 = 1
//disable it from boot

  sudo vi /etc/default/grub

//edit the line
  GRUB_CMDLINE_LINUX_DEFAULT="ipv6.disable=1 quiet splash"

//update grub

  sudo update-grub2

  sudo ufw disable

//updating /etc/hosts
  echo "192.168.200.10 namenode" | sudo tee /etc/hosts
  echo "192.168.200.11 datanode1" | sudo tee -a /etc/hosts
  echo "192.168.200.12 datanode2" | sudo tee -a /etc/hosts
  echo "127.0.0.1 localhost" | sudo tee -a /etc/hosts

//Install Java
  
  sudo apt-get update
  sudo apt-get install openjdk-8-jdk

  java -version

  ls -l /usr/bin/java
  ls -l /etc/alternatives/java

//set java home

  echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64" | sudo tee -a /etc/environment
  echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64" | sudo tee -a /etc/profile


//ZFS
  sudo apt install zfs
  sudo zpool create -f datapool /dev/sdc /dev/sdd /dev/sde
  df -h
  sudo zpool status
  sudo zfs create datapool/hadoop
  sudo zfs set mountpoint=/usr/local/hadoop  datapool/hadoop
 
  sudo zfs create datapool/hadoop/home
  sudo zfs set mountpoint=/home/hadoop datapool/hadoop/home
  sudo groupadd hadoop
  sudo useradd -s /bin/bash -g hadoop -G sudo -d /home/hadoop hadoop                                                                    

  sudo chown -R hadoop:hadoop /home/hadoop
  sudo chown -R hadoop:hadoop /usr/local/hadoop/
  sudo passwd hadoop

  sudo zfs create datapool/hadoop/tmp
  sudo zfs create datapool/hadoop/tmp/dfs
  sudo zfs create datapool/hadoop/tmp/dfs/name
  sudo zfs create datapool/hadoop/tmp/dfs/namesecondary
 
  sudo zfs create datapool/hadoop/tmp/yarn
  sudo zfs create datapool/hadoop/tmp/yarn/yarn-nm-recovery
  sudo zfs create datapool/hadoop/hdfs
  sudo zfs create datapool/hadoop/hdfs/data
  sudo zfs create datapool/hadoop/yarn
  sudo zfs create datapool/hadoop/yarn/local
  sudo zfs create datapool/hadoop/yarn/log
     
  sudo chown -R hadoop:hadoop /usr/local/hadoop



//install ssh & rsync
sudo apt-get install openssh-server


//Enable passwordless sudo
  echo "hadoop ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers


//Restart the VM
login as hadoop

//Download Hadoop
  wget -c http://redrockdigimark.com/apachemirror/hadoop/common/hadoop-2.8.0/hadoop-2.8.0.tar.gz
 
  tar -xvf hadoop-2.8.0.tar.gz

   mv /home/hadoop/hadoop-2.8.0/* /usr/local/hadoop/

  ls /usr/local/hadoop

//Configuration Files
vi ~/.bashrc

#HADOOP VARIABLES START

    export JAVA_HOME=/usr/lib/jvm/java-8-oracle

    export HADOOP_INSTALL=/usr/local/hadoop

    export PATH=$PATH:$HADOOP_INSTALL/bin

    export PATH=$PATH:$HADOOP_INSTALL/sbin

    export HADOOP_MAPRED_HOME=$HADOOP_INSTALL

    export HADOOP_COMMON_HOME=$HADOOP_INSTALL

    export HADOOP_HDFS_HOME=$HADOOP_INSTALL

    export YARN_HOME=$HADOOP_INSTALL/yarn

    export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_INSTALL/lib/native

    export HADOOP_OPTS="-Djava.library.path=$HADOOP_INSTALL/lib/native"

#HADOOP VARIABLES END


 vi ~/.profile

#HADOOP VARIABLES START

    export JAVA_HOME=/usr/lib/jvm/java-8-oracle

    export HADOOP_INSTALL=/usr/local/hadoop

    export PATH=$PATH:$HADOOP_INSTALL/bin:HADOOP_INSTALL/sbin

    export HADOOP_MAPRED_HOME=$HADOOP_INSTALL

    export HADOOP_COMMON_HOME=$HADOOP_INSTALL

    export HADOOP_HDFS_HOME=$HADOOP_INSTALL

    export YARN_HOME=$HADOOP_INSTALL/yarn

    export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_INSTALL/lib/native

    export HADOOP_OPTS="-Djava.library.path=$HADOOP_INSTALL/lib/native"

#HADOOP VARIABLES END


sudo vi /etc/profile

#HADOOP VARIABLES START

    export JAVA_HOME=/usr/lib/jvm/java-8-oracle

    export HADOOP_INSTALL=/usr/local/hadoop

    export PATH=$PATH:$HADOOP_INSTALL/bin

    export PATH=$PATH:$HADOOP_INSTALL/sbin

    export HADOOP_MAPRED_HOME=$HADOOP_INSTALL

    export HADOOP_COMMON_HOME=$HADOOP_INSTALL

    export HADOOP_HDFS_HOME=$HADOOP_INSTALL

    export YARN_HOME=$HADOOP_INSTALL/yarn

    export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_INSTALL/lib/native

    export HADOOP_OPTS="-Djava.library.path=$HADOOP_INSTALL/lib/native"

#HADOOP VARIABLES END


cd /usr/local/hadoop/etc/hadoop

vi core-site.xml

      <configuration>
      <property>
          <name>fs.defaultFS</name>
          <value>hdfs://namenode:9000</value>
      </property>
      <property>
          <name>dfs.permissions</name>
          <value>false</value>
      </property>
      <property>
          <name>hadoop.tmp.dir</name>
          <value>/usr/local/hadoop/tmp</value>
          <description>A base for other temporary
          directories.</description>
      </property>
      </configuration>

vi hadoop-env.sh
      export JAVA_HOME=/usr/lib/jvm/java-8-oracle

vi hdfs-site.xml
      <configuration>
<property>
<name>dfs.replication</name>
<value>1</value>
</property>
<property>
<name>dfs.blocksize</name>
<value>64m</value>
</property>
<property>
<name>dfs.namenode.name.dir</name>
<value>file:///usr/local/hadoop/tmp/dfs/name</value>
</property>
<property>
<name>dfs.datanode.data.dir</name>
<value>file:///usr/local/hadoop/hdfs/data</value>
</property>
<property>
<name>dfs.namenode.checkpoint.dir</name>
<value>file:///usr/local/hadoop/tmp/dfs/namesecondary</value>
</property>
<property>
<name>dfs.namenode.checkpoint.edits.dir</name>
<value>file:///usr/local/hadoop/tmp/dfs/namesecondary</value>
</property>
</configuration>

cp mapred-site.xml.template mapred-site.xml

vi mapred-site.xml
 <configuration>
<property>
<name>mapreduce.jobtracker.address</name>
<value>namenode:54311</value>
<description>The host and port that the MapReduce job tracker
runs
at. If “local”, then jobs are run in-process as a single map
and reduce task.
</description>
</property>
<property>
<name>mapreduce.framework.name</name>
<value>yarn</value>
</property>
</configuration>

vi yarn-site.xml
  <configuration>
<property>
<name>yarn.nodemanager.aux-services</name>
<value>mapreduce_shuffle</value>
</property>
<property>
<name>yarn.resourcemanager.scheduler.address</name>
<value>namenode:8030</value>
</property>
<property>
<name>yarn.resourcemanager.address</name>
<value>namenode:8032</value>
</property>
<property>
<name>yarn.resourcemanager.webapp.address</name>
<value>namenode:8088</value>
</property>
<property>
<name>yarn.resourcemanager.resource-tracker.address</name>
<value>namenode:8031</value>
</property>
<property>
<name>yarn.resourcemanager.admin.address</name>
<value>namenode:8033</value>
</property>
</configuration>

// Export Vm

//Change the ip of both dataNode  vm

login to the datanode1
$ sudo vi /etc/network/interfaces
change the ip Address
192.168.200.11

login to the datanode2
$ sudo vi /etc/network/interfaces
change the ip Address
192.168.200.12



//Change Hostname on each node
Login to Namenode & run the command
  echo "namenode" | sudo tee /etc/hostname
Login to Datanode1 & run the command
  echo "datanode1" | sudo tee /etc/hostname
Login to Datanode2 & run the command
  echo "datanode2" | sudo tee /etc/hostname

//Restart VMs

//For namenode


echo "192.168.200.10" > /usr/local/hadoop/etc/hadoop/masters

echo "192.168.200.11" > /usr/local/hadoop/etc/hadoop/slaves

echo "192.168.200.12" >> /usr/local/hadoop/etc/hadoop/slaves


//For datanode1
echo "192.168.200.11" > /usr/local/hadoop/etc/hadoop/slaves

//For datanode2
echo "192.168.200.12" > /usr/local/hadoop/etc/hadoop/slaves


//Passwordless SSH
(Run on Namenode)
for i in 0 1 2;do ssh hadoop@192.168.200.1$i 'ssh-keygen -t rsa -P "" ' ;done

for i in 0 1 2;do cat .ssh/id_rsa.pub | ssh hadoop@192.168.200.1$i 'cat>> .ssh/authorized_keys' ;done
for i in 0 1 2; do ssh hadoop@192.168.200.1$i ;done

//Start hadoop
Format -
   hadoop namenode -format

   start-all.sh; hdfs dfsadmin -report; yarn node -list

   hadoop jar /usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.8.0.jar pi 1 10
